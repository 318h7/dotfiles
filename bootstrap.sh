#!/usr/bin/env bash

# Setup Doom Vimpul Zhs Syntax Highlight
read -p "Clone git projects? [y/n]" -n 1 -r
echo

if [[ $REPLY =~ ^[Yy]$ ]]; then
  echo "Cloning projects..."

  # Vim Plug
  curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

  # Clone and install FZ
  git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
  ~/.fzf/install

  # Emacs Doom!
  git clone https://github.com/hlissner/doom-emacs ~/.emacs.d
  ~/.emacs.d/bin/doom install

  # Oh-My-Zsh
  sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

  # Zsh Syntax higlight
  git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ~/.zsh-syntax-highlighting

  echo "Done!"
fi

# Create symlinks
read -p "Cleate symlinks? [y/n]" -n 1 -r
echo

if [[ $REPLY =~ ^[Yy]$ ]]; then
  echo "Making symbolic links..."
  sh ./setup/make-symlinks.sh
  echo "Done!"
fi

# Install dependencies
read -p "Install packages? [y/n]" -n 1 -r
echo

if [[ $REPLY =~ ^[Yy]$ ]]; then
  echo "Installing packages..."
  sh ./setup/install-dependencies.sh
  echo "Done!"
fi

# Restart X server
read -p "Restart X? [y/n]" -n 1 -r
echo

if [[ $REPLY =~ ^[Yy]$ ]]
then
    echo "See you in a new world!"
    exec pkill X
fi
