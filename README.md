### Arch Linux dotfiles project

#### Stack
* Desktop        -  i3-gaps
* Status bar     -  polybar
* File Manager   -  PCManFM
* Music player   -  spotifyd
* Notifications  -  dunst
* DisplayManager -  lxrandr
* Network        -  nmtui
* Sound          -  paxmixer
* Login          -  arandr
* IDE            -  Doom

#### Usage

##### On a new machine run
```Bash
cd ~ &&\
git clone https://gitlab.com/aleksandr.voronovic/dotfiles .dotfiles &&\
cd .dotfiles &&\
sudo chmod +x bootsratp.sh &&\
./bootsratp.sh
```

This will install the packages and create the symbolic lincs for the configuration files

##### To update the configuration
Edit the configuration

```Bash
cd .dotfiles
git add .
git commit -m ''
git push
```

