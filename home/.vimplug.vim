"" Load Plugins
call plug#begin('~/.vim/plugged') 

" Show colors
Plug 'chrisbra/Colorizer'
" Surround 
Plug 'tpope/vim-surround'
" Fancy status bar
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
" File search
Plug 'ctrlpvim/ctrlp.vim'
" File Tree
Plug 'scrooloose/nerdtree'
" Repeat commands
Plug 'tpope/vim-repeat'
" Solarized color scheme
" Plug 'altercation/vim-colors-solarized'
" Plug 'lifepillar/vim-solarized8'
"*****************************************
" Meke me IDE
"*****************************************

"Better js syntax
Plug 'pangloss/vim-javascript'
" JSX syntax
Plug 'mxw/vim-jsx'
" Code Formatting tools
" Plug 'prettier/prettier'
" Linter plugin
" Plug 'w0rp/ale'
" Do magic with HTML Ctrl + Y + ',' for magic
Plug 'mattn/emmet-vim'
" Custom code snippets !
" Plug 'SirVer/ultisnips'
"*****************************************
call plug#end()
