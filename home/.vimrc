"" Set non compatible
set nocompatible

"" Load VimPlug
source $HOME/.vimplug.vim

"*****************************************************************************
"" Basic Setup
"*****************************************************************************"

"" Encoding
set encoding=utf-8
set binary
set ttyfast

"" Fix backspace indent
set backspace=indent,eol,start

"" Tabs. May be overriten by autocmd rules
set tabstop=2
set softtabstop=0
set shiftwidth=2
set expandtab

"" Map leader to ,
"" let mapleader=','

"" Enable hidden buffers
set hidden

"" Searching
set hlsearch
set incsearch
set ignorecase
set smartcase

"" Directories for swp files
set nobackup
set noswapfile

set fileformats=unix,dos,mac

if exists('$SHELL')
    set shell=$SHELL
else
    set shell=/bin/sh
endif

" session management
let g:session_directory = "~/.vim/session"
let g:session_autoload = "no"
let g:session_autosave = "no"
let g:session_command_aliases = 1

" Set project search path
set path+=**
set wildignore+=tags,*/tmp/*,*.so,*.swp,*.zip,*/node_modules/*,*/build/*

" Display available files
" set wildmenu

" Omnicompletion
set omnifunc=syntaxcomplete#Complete

set clipboard=unnamed

"*****************************************************************************
"" Visual Settings
"*****************************************************************************
set ruler
set number relativenumber

set mousemodel=popup
set mouse=a

"" True Color
" set termguicolors
set t_Co=256
set term=xterm-256color
set background=dark

"colorscheme default
syntax on

hi! LineNr ctermfg=grey
hi! Statement ctermfg=red ctermbg=NONE

"" Disable the blinking cursor.
set gcr=a:blinkon0
set scrolloff=5

"" Status bar
set laststatus=2

"" Use modeline overrides
set modeline
set modelines=10
let g:airline_theme='term'

set title
set titleold="Terminal"
set titlestring=%F

set statusline=%F%m%r%h%w%=(%{&ff}/%Y)\ (line\ %l\/%L,\ col\ %c)\

" Search mappings: These will make it so that going to the next one in a
" search will center on the line it's found in.
nnoremap n nzzzv
nnoremap N Nzzzv

" vim-airline
"let g:airline_theme = 'powerlineish'
let g:airline_powerline_fonts = 1
let g:airline#extensions#branch#enabled = 1
let g:airline#extensions#tabline#enabled = 1

"*****************************************************************************
"" Abbreviations
"*****************************************************************************
"" no one is really happy until you have this shortcuts
cnoreabbrev W! w!
cnoreabbrev Q! q!
cnoreabbrev Qall! qall!
cnoreabbrev Wq wq
cnoreabbrev Wa wa
cnoreabbrev wQ wq
cnoreabbrev WQ wq
cnoreabbrev W w
cnoreabbrev Q q
cnoreabbrev Qall qall

"*****************************************************************************
""  Plugins additional
"*****************************************************************************

" Syntax highlighting for Flow [vim-javascript]
let g:javascript_plugin_flow = 1
 
"*****************************************************************************
"" Key Bindings
"*****************************************************************************
" Toggle show colors
nmap <F2> :ColorToggle<CR>
" Copy to system clipboard in VISUAL mode
vmap <F7> y<CR>:call system('xclip', @0)<CR>
vmap <F8> y<CR>:call system('google-chrome-stable @0')<CR>
" :vimgrep word under cursor
map <leader>g :execute "vimgrep /".expand("<cword>")."/gj **" <Bar> cw<CR>
" Navigate quickfixes
map <C-j> :cn<CR>
map <C-k> :cp<CR>
" Replace text under cursot
nmap <leader>r yiw:%s/<c-r>"//g<left><left>
vmap <leader>r y:%s/<c-r>"//g<left><left>
" Replace text in multiple files
nmap <leader>m yiw:argadd `grep -r -l <c-r>" .`<CR>
vmap <leader>m y::argadd `grep -r -l <c-r>" .`<CR>
nmap <leader>d yiw:argdo %s/<c-r>"//g<left><left>
vmap <leader>d y:argdo %s/<c-r>"//g<left><left>
" open file in chrome
nnoremap <leader>c :silent exec "!google-chrome-stable %"<CR>:redraw!<CR>
" navigate splits more efectively
nmap <Tab> :bNext<CR>
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
" bb arrows
noremap <Up> <Nop>
noremap <Down> <Nop>
noremap <Left> <Nop>
noremap <Right> <Nop>
" navigate buffers
noremap <Tab> :bnext<CR>

"*************************************
"" Silence!
"*************************************
set noeb vb t_vb=
