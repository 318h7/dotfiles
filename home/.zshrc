# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH
export ZSH="$HOME/.oh-my-zsh"

DEFAULT_USER=`whoami`

ZSH_THEME="agnoster"
# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
#COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
#  under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# You may need to manually set your language environmen
export LANG=en_US.UTF-8


######################################################################
# Plugins
######################################################################
plugins=(
  npm git docker extract yarn-completion nordvpnteams
)

#########
source $ZSH/oh-my-zsh.sh

######################################################################
# Nifty Scripts
######################################################################

function repeat() {
    number=$1
    shift
    for n in $(seq $number); do
      $@
    done
} 

######################################################################
# Aliases
######################################################################
  # System
  alias grep='grep --colour=auto'
  alias t=touch
  alias cp="cp -i"
  alias cls=clear
  alias more=less
  alias mkd="mkdir -pv"
  alias ss="sudo systemctl"
  alias ssl="sudo systemctl list-units"
  alias ka=killall
  alias ls="ls -hN --color=auto --group-directories-first"
#  alias mix=pacmixer
  alias mix="pulsemixer"
  alias cal="cal -m"
  alias dmenu="dmenu -dmenu"

  # Vim
  alias v="vim"
  alias sv="sudo -e"

  # Misc
  alias fm="pcmanfm . &"
  alias e="emacs -nw"
  alias rge="rg --files --hidden -i -L 2>/dev/null"
  alias fzfp="fzf --preview 'bat --style=numbers --color=always {}'"
  alias ff="rge | fzfp | ifne xargs -o vim "
  alias z="zathura --fork"
  #alias pass="passmenu -p 'pass'"

  ## Configs
  alias i3c="v $HOME/.config/i3/config"
  alias pp="v $HOME/.config/polybar/config"
  alias xx="v $HOME/.Xresources"
  alias zz="v $HOME/.zshrc"
  alias vv="v $HOME/.vimrc"
  alias kk="v $HOME/.config/kitty/kitty.conf"
  alias cc="v $HOME/.config/picom/picom.conf"
  alias gg="v $HOME/.gitconfig"

  # Pacman
  alias p="sudo pacman --color always --needed"
  alias yay="yay --color always --nocleanmenu --nodiffmenu"
  alias pfi="yay -Slq | fzf -m --preview 'yay -Si {1}' | xargs -ro yay -S --needed --color always"
  alias pfr="yay -Qq | fzf -m --preview 'yay -Si {1}' | xargs -ro yay -R --color always"
  alias pffr="yay -Qq | fzf -m --preview 'yay -Si {1}' | xargs -ro yay -Rdd --colod always"

  # Git
  alias gaa="git add ."
  alias gs="git status"
  alias gc="git checkout"
  alias gcl="git clone"
  alias gr="git restore"
  alias grt="git reset"
  alias gc="git commit -m"
  alias gca="git commit --amend"
  alias gl="git pull"
  alias gp="git push"

  # Tex
  alias lw="latexmk -pdf -pvc "

  alias nvt="nordvpnteams"

  ## Use default node env
  alias n="nvm use default"
  alias yar="yarn run"
  alias yad="yarn develop"
  alias yat="yarn typecheck"
  alias yal="yarn lint --fix"

  ## Node memory hack
  alias node="node --max-old-space-size=2000"

  # Folder Aliases
  hash -d CORE=~/dev/frontend/core
  hash -d COMMON=~/dev/frontend/common
  hash -d CONFIGS=~/dev/frontend/configs
  hash -d AUTH=~/dev/frontend/auth
  hash -d WB=~/dev/frontend/web
  hash -d CP=~/dev/frontend/cp
  hash -d SUPPORT=~/dev/frontend/support

######################################################################
# Exports
######################################################################

export WINEARCH=win32 
export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" --no-use # This loads nvm

# export CHROME_DEVEL_SANDBOX=/usr/local/sbin/chrome-devel-sandbox
# export CHROME_DRIVER=/usr/local/sbin/chromedriver
export VISUAL="vim"
export EDITOR="vim"
export TERM='xterm-256color'
export SUDO_ASKPASS=~/.local/bin/askpass
export PAGER="less"

# Clipmenu
#export CM_LAUNCHER=fzf
export CM_LAUNCHER=rofi
export CM_HISTLENGTH=20
export CM_DIR="/tmp"

# Add scripts to path
export PATH=$PATH:~/.local/bin
export PATH=$PATH:~/bin

# Add Doom executable to path
export PATH=$PATH:~/.emacs.d/bin
export PATH="$PATH:$(ruby -e 'puts Gem.user_dir')/bin"

###################################################################
# Misc
##################################################################

# Enable Vim mode in ZSH
# bindkey -v

# disable that darn sound beep
unsetopt BEEP
unsetopt LIST_BEEP

#################################################################
# FZF Magic ✨
#################################################################

# Options to fzf command
export FZF_COMPLETION_OPTS='-i'
export FZF_DEFAULT_COMMAND="rg --files --hidden -i -L 2>/dev/null"
export FZF_CTRL_T_COMMAND=$FZF_DEFAULT_COMMAND
export FZF_ALT_C_COMMAND="fd --type d --hidden -i -L"

# Use fd (https://github.com/sharkdp/fd) instead of the default find
# command for listing path candidates.
# - The first argument to the function ($1) is the base path to start traversal
# - See the source code (completion.{bash,zsh}) for the details.
_fzf_compgen_path() {
  fd --hidden -i --exclude . "$1"
}

# Use fd to generate the list for directory completion
_fzf_compgen_dir() {
  fd --type d --hidden -i --exclude . "$1"
}

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

###################################################################

# Enable syntax highlight
source ~/.zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
