;;; .doom.d/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here

;;; Theme CONFIG
(require 'doom-themes)

;;; Global settings (defaults)
(setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
      doom-themes-enable-italic t) ; if nil, italics is universally disabled

;; Load the theme (doom-one, doom-molokai, etc); keep in mind that each theme
;; may have their own settings.
(load-theme ' doom-spacegrey)

;; Enable custom neotree theme (all-the-icons must be installed!)
(doom-themes-neotree-config)
;; or for treemacs users
(setq doom-themes-treemacs-theme "doom-colors") ; use the colorful treemacs theme
(doom-themes-treemacs-config)

;; Corrects (and improves) org-mode's native fontification.
(doom-themes-org-config)

(setq projectile-project-search-path '("~/dev/"))

;;; Refresh buffer when file changes on disk.
(global-auto-revert-mode t)
(setq auto-revert-check-vc-info t)

;; Refresh tags and project cache on branch switch
;;

;; stop asking to replace TAGS file
(setq tags-revert-without-query 1)

;; refresh function
(defun refresh-project (&rest _args)
  ;; We ignore the args to `magit-checkout'.
  (projectile-invalidate-cache nil)
  (projectile-regenerate-tags ))

;; magit function advices
(advice-add 'magit-checkout
            :after #'refresh-project)
(advice-add 'magit-branch-and-checkout ; This is `b c'.
            :after #'refresh-project)

;; Maggit exclude from TODO search
(setq magit-todos-exlude-globs '("node_modules/*", "build/*", "dist/*", "reports/*", "coverage/*"))

;;; FONTS
(setq doom-font (font-spec :family "Source Code Pro" :size 14))
;; (setq doom-font (font-spec :family "Iosevka" :size 14))
;; (setq doom-font (font-spec :family "Fira Code Symbol" :size 14 :width 'condenced))
;; (pretty-code +fira)


;; triggered refresh
;; projectile invalidate
;; rebuild tags
;;

;; ESLint funcs
(defun eslint-fix-file ()
  (interactive)
  (shell-command
   (concat
      "tslint --fix -q "
      (buffer-file-name)))
   (revert-buffer t t))

(global-set-key (kbd "C-c t") 'tslint-fix-file)

;;; TS
(require 'flycheck)
(defun setup-tide-mode ()
  (interactive)
  (tide-setup)
  (flycheck-mode +1)
  (setq flycheck-check-syntax-automatically '(save mode-enabled))
  (flycheck-add-next-checker 'typescript-tide '(t . typescript-tslint) 'append)
  (tide-hl-identifier-mode +1)
  (eldoc-mode +1)
  (company-mode +1))

(add-hook 'typescript-mode-hook #'setup-tide-mode)

(require 'web-mode)
(add-to-list 'auto-mode-alist '("\\.tsx\\'" . web-mode))
(add-hook 'web-mode-hook
          (lambda ()
            (when (string-equal "tsx" (file-name-extension buffer-file-name))
              (setup-tide-mode))))
;; enable typescript-tslint checker
(flycheck-add-mode 'typescript-tslint 'web-mode)

;; aligns annotation to the right hand side
(setq company-tooltip-align-annotations t)
(setq company-idle-delay 0)

;;; Space / TABS
(setq typescript-indent-level 2)
(setq js-indent-level 2)
(setq web-mode-code-indent-offset 2)

;; set tide formatting
(setq-default tide-format-options '(:indentStyle "Smart" :baseIndentSize 2 :convertTabsToSpaces t :tabSize 2 :indentSize 2))

;;; Tutor - steal best things from vim
(defun doomtutor()
  "Run vimtutor in emacs

This is a vim tutor file copy
for Evil emacs users to play around"
  (interactive)
  (when (file-exists-p "/usr/share/vim/vim81/tutor/tutor")
    (setq temp-file-name
      (concat
        "/tmp/doom-tutor-"
        (number-to-string (random (expt 16 4)))
        (number-to-string (random (expt 16 4)))))
    (copy-file
      "/usr/share/vim/vim81/tutor/tutor"
      temp-file-name)
    (find-file temp-file-name)
    (goto-char (point-min))))


;;; TAB char
(progn
  ;; make indentation commands use space only (never tab character)
  (setq-default indent-tabs-mode nil)
  ;; emacs 23.1 to 26, default to t
  ;; if indent-tabs-mode is t, it means it may use tab, resulting mixed space and tab
  (setq-default c-basic-indent 2)
  (setq-default tab-width 2)
  ;; make tab key call indent command or insert tab character, depending on cursor position
  (setq-default tab-always-indent nil)
)

;;; Pretty Ligatures
;; (require 'pretty-mode)
;; (global-pretty-mode t)

;; (pretty-deactivate-groups
;;  '(:equality :ordering :ordering-double :ordering-triple
;;              :arrows :arrows-twoheaded :punctuation
;;              :logic :sets))

;; (pretty-activate-groups
;;   '(:sub-and-superscripts :arrows :logic :greek :arithmetic-nary))

;;; Shell colors
(add-hook 'shell-mode-hook 'ansi-color-for-comint-mode-on)
(add-to-list 'comint-output-filter-functions 'ansi-color-process-output)
(require 'ansi-color)

(defun colorize-compilation-buffer ()
  (let ((inhibit-read-only t))
    (ansi-color-apply-on-region (point-min) (point-max))))

; (add-hook 'compilation-filter-hook 'colorize-compilation-buffer)
