#!/usr/bin/env bash

[ ! -d "$HOME/Pictures/Screencasts" ] && mkdir -p "$HOME/Pictures/Screencasts"


TMPFILE="$(mktemp -t screencast-XXXXXXX)".mkv
OUTPUT="$HOME/Pictures/Screencasts/$(date +%F-%H-%M-%S)"

read -r X Y W H G ID < <(slop -f "%x %y %w %h %g %i")
ffmpeg -f x11grab -s "$W"x"$H" -i :0.0+$X,$Y "$TMPFILE"

notify-send 'GIF: Generating palette...'
ffmpeg -y -i "$TMPFILE"  -vf fps=10,palettegen /tmp/palette.png
notify-send 'GIF: generating gif...'
#ffmpeg -i "$TMPFILE" -r 20 -pix_fmt rgb24 $OUTPUT.gif
ffmpeg -i "$TMPFILE" -r 15 -i /tmp/palette.png -filter_complex "paletteuse" $OUTPUT.gif
#mv $TMPFILE $OUTPUT.mkv

notify-send "GIF: size $(du -h $OUTPUT.gif | awk '{print $1}')"

rm -f $TEMPFILE
