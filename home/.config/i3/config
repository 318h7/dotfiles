# Import colors
set_from_resource  $background     background   #000000
set_from_resource  $foreground     foreground   #000000

set_from_resource  $black          color0       #000000
set_from_resource  $dblack         color8       #000000
set_from_resource  $red            color1       #000000
set_from_resource  $dred           color9       #000000
set_from_resource  $green          color2       #000000
set_from_resource  $dgreen         color10      #000000
set_from_resource  $yellow         color3       #000000
set_from_resource  $dyellow        color11      #000000
set_from_resource  $blue           color4       #000000
set_from_resource  $dblue          color12      #000000
set_from_resource  $magneta        color5       #000000
set_from_resource  $dmagneta       color13      #000000
set_from_resource  $cyan           color6       #000000
set_from_resource  $dcyan          color14      #000000
set_from_resource  $white          color7       #000000
set_from_resource  $dwhite         color15      #000000

set $wwhite #ffffff

# define main output

# Set Mod key Power
set $mod Mod4

# variable
set $wallpaper "$HOME/.wallpapers/sandstone.png"

# Font for window titles. Will also be used by the bar unless a different font
# is used in the bar {} block below.
font pango:Fira Code,FontAwesome 10

# Set colors
#                        BORDER       BACKGROUND   TEXT        INDICATOR    CHILD_BORDER
client.focused           $blue        $blue        $wwhite     $blue        $blue
client.unfocused         $foreground  $foreground  $wwhite     $foreground  $foreground
client.focused_inactive  $foreground  $foreground  $wwhite     $foreground  $foreground 
client.urgent            $dred        $dred        $wwhite     $dred        $dred
client.background        $background

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# start a terminal
bindsym $mod+Return exec kitty

# kill focused window
bindsym $mod+Shift+q kill

# mimic Alt+Tab
bindsym Mod1+Tab workspace back_and_forth

# start dmenu (a program launcher)

bindsym $mod+space exec rofi -show "combi" -display-combi '>'
bindsym Control+space exec rofi -show "drun" -run-command 'sudo {cmd}' -display-drun 'SUDO> '

#*********************
# Custom Key Bindings
#*********************

# lock screen
bindsym $mod+Escape exec lock

# rofi power
bindsym $mod+Delete exec rofi-power $(echo -e 'Sleep\nRestart\nHibernate\nPoweroff' | rofi -i -dmenu -p "power")

# make screenshots
bindsym Print exec maim ~/Pictures/Screenshots/$(date +%s).png
bindsym --release Control+Print exec maim -i $(xdotool getactivewindow) ~/Pictures/Screenshots/$(date +%s).png
bindsym  Control+Shift+Print exec maim -s | xclip -selection clipboard -t image/png

# pick color
bindsym --release $mod+Shift+c exec --no-startup-id colorpicker --short --one-shot | xclip

# cast screen to gif
bindsym --release $mod+p exec ~/.config/i3/capture-gif.sh
bindsym --release $mod+Shift+p exec killall ffmpeg

# Move to last urgent
bindsym $mod+x [urgent=latest] focus

# Call network menu
bindsym $mod+n exec kitty --class nmtui nmtui connect

# Wireless menu
bindsym $mod+m exec arandr

# Passwords
bindsym $mod+Insert exec passmenu -dmenu -p "pass"

# VPN
bindsym $mod+Shift+n exec toggle-vpn Whitelabel_tcp

# clipmenu
bindsym $mod+c exec clipmenu -p "cliboard"

# media keys
bindsym XF86AudioPlay exec playerctl play-pause
bindsym XF86AudioPause exec playerctl play-pause
bindsym XF86AudioNext exec playerctl next
bindsym XF86AudioPrev exec playerctl previous
bindsym XF86AudioStop exec playerctl stop

# toggle display
bindsym XF86Display exec toggle-display

# Brightness
bindsym XF86MonBrightnessUp exec xbacklight -inc 10 # increase screen brightness
bindsym XF86MonBrightnessDown exec xbacklight -dec 10 # decrease screen brightness

# Volume
bindsym XF86AudioRaiseVolume exec --no-startup-id amixer sset Master 5%+ 
bindsym XF86AudioLowerVolume exec --no-startup-id amixer sset Master 5%- 


#VPN
#bindsym $mod+v exec --no-startup-id toggle-vpn Whitelabel_tcp
#*******************
# i3 Key Bindings
#******************

# change focus
bindsym $mod+h focus left
bindsym $mod+j focus down
bindsym $mod+k focus up
bindsym $mod+l focus right

# alternatively, you can use the cursor keys:
#ibindsym $mod+Left focus left
#bindsym $mod+Down focus down
#bindsym $mod+Up focus up
#bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+h move left
bindsym $mod+Shift+j move down
bindsym $mod+Shift+k move up
bindsym $mod+Shift+l move right

# alternatively, you can use the cursor keys:
#bindsym $mod+Shift+Left move left
#bindsym $mod+Shift+Down move down
#bindsym $mod+Shift+Up move up
#bindsym $mod+Shift+Right move right

# split in horizontal orientation
bindsym $mod+s split v

# split in vertical orientation
bindsym $mod+v split h

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
# bindsym $mod+s layout stacking
bindsym $mod+t layout tabbed
bindsym $mod+w layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
#bindsym $mod+space focus mode_toggle

# focus the parent container
#bindsym $mod+a focus parent

# focus the child container
#bindsym $mod+d focus child

# Define names for default workspaces for which we configure key bindings later on.
# We use variables to avoid repeating the names in multiple places.

set $ws1  "1:"
set $ws2  "2:"
set $ws3  "3:"
set $ws4  "4:"
set $ws5  "5:"
set $ws6  "6:"
set $ws7  "7"
set $ws8  "8"
set $ws9  "9"
set $ws10 "10"

# bind apps to workspaces
# assign [class="jetbrains-idea"] $ws1 
assign [class="Emacs"] $ws2
assign [class="firefox"] $ws3
assign [class="Spotify"] $ws4
assign [class="Slack"] $ws5


# assign floating apps 
for_window [class="nmtui"] floating enable
#for_window [class="clipmenu"] floating enable
for_window [class="Arandr"] floating enable
for_window [class="Pcmanfm"] floating enable
for_window [class="Lxappearance"] floating enable
for_window [class="Io.elementary.files"] floating enable

floating_maximum_size 1920 x 1080 

# redo outputs
# bind workspaces to monitors
#workspace $ws1 output HDMI-1exec 
#workspace $ws2 output DP-1
#workspace $ws3 output HDMI-1

# switch to workspace
bindsym $mod+1 workspace $ws1
bindsym $mod+2 workspace $ws2
bindsym $mod+3 workspace $ws3
bindsym $mod+4 workspace $ws4
bindsym $mod+5 workspace $ws5
bindsym $mod+6 workspace $ws6
bindsym $mod+7 workspace $ws7
bindsym $mod+8 workspace $ws8
bindsym $mod+9 workspace $ws9
bindsym $mod+0 workspace $ws10

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace $ws1
bindsym $mod+Shift+2 move container to workspace $ws2
bindsym $mod+Shift+3 move container to workspace $ws3
bindsym $mod+Shift+4 move container to workspace $ws4
bindsym $mod+Shift+5 move container to workspace $ws5
bindsym $mod+Shift+6 move container to workspace $ws6
bindsym $mod+Shift+7 move container to workspace $ws7
bindsym $mod+Shift+8 move container to workspace $ws8
bindsym $mod+Shift+9 move container to workspace $ws9
bindsym $mod+Shift+0 move container to workspace $ws10

# reload the configuration file
#bindsym $mod+Shift+c reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart
# exit i3 (logs you out of your X session)
bindsym $mod+Shift+Escape exec "i3-msg exit"

# resize window (you can also use the mouse for that)
mode "resize" {
        # These bindings trigger as soon as you enter the resize mode

        # Pressing left will shrink the window’s width.
        # Pressing right will grow the window’s width.
        # Pressing up will shrink the window’s height.
        # Pressing down will grow the window’s height.
        bindsym h resize shrink width 10 px or 10 ppt
        bindsym j resize grow height 10 px or 10 ppt
        bindsym k resize shrink height 10 px or 10 ppt
        bindsym l resize grow width 10 px or 10 ppt

        # back to normal: Enter or Escape or $mod+r
        bindsym Return mode "default"
        bindsym Escape mode "default"
        bindsym $mod+r mode "default"
}

bindsym $mod+r mode "resize"

# custom config
hide_edge_borders smart
mouse_warping none
focus_follows_mouse no
focus_on_window_activation smart
workspace_auto_back_and_forth yes

# Gaps
#smart_gaps on
smart_borders on
gaps inner 10
gaps outer -2
gaps top 22
#gaps bottom -4

# Disable titlebars
for_window [class=".*"] border pixel 0

# Compton
exec --no-startup-id picom -b --no-fading-openclose 

# Resolution
exec --no-startup-id xrandr --auto

# Sleep transfer
exec --no-startup-id xss-lock -n ~/.config/i3/dim-screen.sh -l ~/.config/i3/transfer-sleep-lock-i3.sh

# Wallpaper
# exec --no-startup-id feh --bg-fill --no-xinerama $wallpaper 
exec --no-startup-id hsetroot -solid "$white"

# Set dunst colors from Xresources
exec_always --no-startup-id dunst\
  -frame_color "black"\
  -lb "$dblack"\
  -lf "$dwhite"\
  -nb "$dblack"\
  -nf "$dwhite"\
  -cb "$dblack"\
  -cf "$red"\

# Polybar
exec --no-startup-id ~/.config/polybar/launch.sh

# Clipmenu
exec --no-startup-id clipmenud
