# Stuff to do

## Config
* Keyboard/Input stuff
* Make /etc/issue

* Make autologin
    /etc/systemd/system/getty@tty1.service.d
    copy overrides file

* Optionally add NVM to installs

/etc/default/grub
```config
GRUB_TIMEOUT=0
GRUB_CMDLINE_LINUX_DEFAULT="quiet udev.log_priority=3 audit=0"
```

* Install GPU driver

* Make i3 scripts executable!!!
* or put them to scripts?

## Laptop?
* powertop
* search ArhcWiki for laprop optimisation package (makes kernel modules)
* Disable SSD service

## GTK stuff?
* add papirus (so far icon theme)

#### Sync time

```Bash
systemd-timesyncd.service
```

Alternative:
```Bash
sudo pacman -S ntp 
systemctl enable ntpd.service
systemctl start ntpd.service
timedatectl set-ntp 1
```

#### See x keyboard arch wiki
* set sysd keyboar
* set x keyboard

#### Misc
* Lookup elementary FM, mb it's good?
* Switch mixer from pacmixer to pulsemixer?
* add script to upgrade dots?

```Bash
  cd .dotfiles
  gaa
  gcm < prompt for message
  gp
```

* Bootstrap home/work setup
 ** prompt for email
 ** use in git settings

