#!/bin/sh

dir=`pwd`

# yay for package management
sudo pacman --noconfirm -S yay

# Installs all the dependecies
sed "/^#\|^$/d" ${dir}/setup/dependencies | sed ':a;N;$!ba;s/\n/ /g' | xargs yay --noconfirm -S

# Post install

# symlink python3 as default
# ln -s /usr/bin/python3 /usr/bin/python

# to fix the mopidy stop bug
# sudo pip uninstall tornado
# sudo pip install tornado==4.4
