#!/usr/bin/env bash

LINKS="`pwd`/setup/links"
DIR="`pwd`/home/"

# sed clean comments and empty lines
# awk frind file names
# make symlink to the config file
echo "Adding config file symlinks..."
sed "/^#\|^$/d" $LINKS | awk -v src="$DIR" -v dest="$HOME/" '$1 ~ /f/ { system("ln -sfn " src $2 " " dest $2)}'

echo "Done!"

# remove all config DIRs
echo "Removing existing configuration folders..."
sed "/^#\|^$/d" $LINKS | awk -v path="$HOME/" '$1 ~ /d/ { system("rm -rf " path $2)}'
echo "Done!"

# sed clean comments and empty lines
# awk find DIR names
# make symlink to config directories
echo "Adding config folder symlinks..."
sed "/^#\|^$/d" $LINKS | awk -v src="$DIR" -v dest="$HOME/" '$1 ~ /d/ { system("ln -sfn " src $2 " " dest $2)}'
echo "Done!"

echo "Adding scripts symlinks..."
DIR="`pwd`/scripts"
BIN="$HOME/.local/bin/"

# Create user bin if does not exist
[ ! -d $BIN ] && mkdir $BIN

ls ${DIR} | awk -v src="$DIR/" -v dest="$BIN" '{system("ln -sfn " src $1 " " dest $1)}'
echo "Done!"



# Make rofi duns symlink!
